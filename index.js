let showHorses;
let showAccount;
let setWager;
let startRacing;
let newGame;

(function notGlobal() {
  let horsesNames = ["Бавария", "Бандит", "Гарольд", "Дантес"];
  let horses = [];
  let wallet = 1000;
  let bets = [];

  function randomInteger(min, max) {
    let time = Math.floor(min + Math.random() * (max + 1 - min));
    return time;
  };

  function createHorse(name) {
    let horse = {
      name: name,
      run: () => {
        return new Promise((resolve) => {
          setTimeout(() => {
            resolve(horse);
          }, randomInteger(500, 3000))
        })
      }
    }
    return horse;
  };

  horsesNames.forEach(name => {
    let horse = createHorse(name);
    horses.push(horse);
  });


  showHorses = function () {
    horsesNames.forEach(horse => console.log(horse));
  };

  showAccount = function () {
    console.log(wallet);
  };

  setWager = function (horse, bet) {
    if (bet > 0) {
      if (bet <= wallet) {
        bets.push({
          name: horse,
          bet: bet
        });
        wallet -= bet;
      } else {
        console.log("Подкопи денег и попробуй снова");
      }
      console.log(bets);
      console.log(`После ставки у тебя осталось ${wallet}`);
    } else {
      console.log(`Ставка должна быть больше 1!`);
    }
  };

  function checkNoWager(bets) {
    console.log(`Это был увлекательный забег! А ещё интереснее он станет, если ты поставишь деньги!`);
  };

  function racingResult(bets, horse) {
    console.log(`Ты ставил:`);
    console.log(bets);
    let moneyDifference = 0;
    bets.forEach(bet => {
      if (bet.name === horse.name) {
        moneyDifference = bet.bet * 2;
        wallet += moneyDifference;
        console.log(`${bet.name}: ставка сыграла! Твой счёт вырос на ${moneyDifference}.`);
      } else {
        console.log(`${bet.name}: увы, не повезло! Ты потерял: ${bet.bet}.`);
      }
    });
    console.log(`Твой счёт после забега: ${wallet}.`);
  };

  function afterRun() {
    if (wallet <= 0) {
      console.log(`У тебя закончились все деньги, больше играть нельзя. Набери «newGame();».`);
    } else {
      console.log(`Сделай ставку через команду «setWager("ИмяЛошади", ставка);»`);
      console.log(`Гонка начнётся по команде «startRacing();»`);
    }
  };

  startRacing = function () {
    let runResults = horses.map((horse) => {
      return horse.run().then((result) => {
        console.log(result.name);
        return result;
      });
    });

    Promise.race(runResults).then((horse) => {
      Promise.all(runResults).then(() => {
        console.log(`Победившая лошадь: ${horse.name}`);
        if (bets.length === 0) {
          checkNoWager(bets);
        } else {
          racingResult(bets, horse);
        }
        bets = [];
        runResults = [];
        afterRun();
      });
    });
  };

  newGame = function () {
    wallet = 1000;
    console.log(`Сегодня в забеге участвуют:`);
    showHorses();
    console.log(`Список лошадей доступен в любой момент по команде «showHorses();»`);
    console.log(`Твой балланс:`);
    showAccount();
    console.log(`Чтобы проверить его в любой момент — используй команду «showAccount();»`);
    console.log(`Сделай ставку через команду «setWager("ИмяЛошади", ставка);»`);
    console.log(`Гонка начнётся по команде «startRacing();»`);
    console.log(`Для начала новой игры используй «newGame();»`);
  };
})();

newGame();